/*global browser:true, laxbreak:true, ko:false*/

var StickerViewModel = function(stickerModel)
{
	this.id = null;
	this.text = ko.observable();
	this.url = ko.observable();
	this.additionalData = ko.observable();
	this.styleBindings =
	{
		left: ko.observable(),
		top: ko.observable(),
		"z-index": ko.observable()
	};

	this.isLinkVisible = ko.computed(function()
	{
		return this.url() != null && this.url().length > 0;
	}, this);

	this.update(stickerModel);
};

StickerViewModel.prototype.update = function(stickerModel)
{
	this.id = stickerModel.id;
	this.text(stickerModel.text);
	this.url(stickerModel.url);
	this.additionalData(stickerModel.additionalData);
	this.styleBindings.left(stickerModel.styleBindings.left);
	this.styleBindings.top(stickerModel.styleBindings.top);
	this.styleBindings["z-index"](stickerModel.styleBindings["z-index"]);
};