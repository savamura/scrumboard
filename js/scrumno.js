/*global $:false, _:false, ko:false*/
$(function() {

	$("#board")
		.droppable()
		.disableSelection();

	$("#stickerPropertiesDialog").dialog({ autoOpen: false });
	$("#stickerPropertiesDialog form").submit(function(e)
	{
		e.preventDefault();
	});

	adjustWindowSize();

	var storageClient = new StorageClient();
	window.boardViewModel = new BoardViewModel(
		{
			boardId: "294b25c6-3266-43f2-8244-220e8dad2c55"
		},
		storageClient);

	window.boardViewModel.on(
		"onShowStickerPropertiesDialog",
		showStickerPropertiesDialog);

	ko.applyBindings(window.boardViewModel);

	storageClient.loadBoard(
		window.boardViewModel.boardId,
		function(xmlHttpRequest)
		{
			window.boardViewModel.updateBoard($.parseJSON(xmlHttpRequest.response));
		},
		function(xhr)
		{
			$.jGrowl("Error calling azure API " + xhr.status + " " + xhr.responseText);
		});
});

function showStickerPropertiesDialog() {
	$("#stickerPropertiesDialog").dialog({dialogClass:"stickerPropertiesDialogClass"});
	$("#stickerPropertiesDialog").dialog("open");
}

ko.bindingHandlers.draggable= {
	init: function(element, valueAccessor) {
		makeDraggable($(element));
	}
};

var makeDraggable = function($element)
{
	$element.draggable({
		appendTo: "#board",
		containment: "#board",
		snap: true,
		snapTolerance: 10,
		stack: ".sticker",
		start: function()
		{
			$(this)
				.removeClass("pointerCursor")
				.addClass("moveCursor")
		},
		stop: function()
		{
			// hack for back properties projection to view model
			var stickerViewModel = ko.dataFor(this);
			var newStyle = {
				top: this.style.top,
				left: this.style.left,
				"z-index": this.style["z-index"]
			};
			stickerViewModel.styleBindings.left(newStyle.left);
			stickerViewModel.styleBindings.top(newStyle.top);
			stickerViewModel.styleBindings["z-index"](newStyle["z-index"]);

			$(this)
				.removeClass("moveCursor")
				.addClass("pointerCursor")
		}
	});

	$element.find("a.workItemLink")
		.addClass("workItemLinkNormal")
		.hover(
			function()
			{
				$(this)
					.removeClass("workItemLinkNormal")
					.addClass("workItemLinkHover");
			},
			function()
			{
				$(this)
					.removeClass("workItemLinkHover")
					.addClass("workItemLinkNormal");
			});


	$element
		.addClass("pointerCursor")
		.dblclick(function()
		{
			showStickerPropertiesDialog();
			var stickerViewModel = ko.dataFor(this);
			var dialogModel = ko.toJS(stickerViewModel);
			dialogModel.isNewSticker = false;
			dialogModel.stickerViewModel = stickerViewModel;
			window.boardViewModel.stickerPropertiesDialog.update(dialogModel);
		});
};

function adjustWindowSize()
{
	$("#board").width(_.max([$(window).width(), $("#board").width()]));
	$("#board").height(_.max([$(window).height(), 700]));
}

function makeGuid()
{
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	});
}

if (typeof console === "undefined")
{
	console = {};
	console.log = function(){};
}