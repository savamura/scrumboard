/*global browser:true, laxbreak:true, ko:false, _:false*/

var BoardViewModel = function(boardModel, storageClient)
{
	_.extend(this, Backbone.Events);
	this.boardId = boardModel.boardId;
	this.stickers = ko.observableArray();
	this.storageClient = storageClient;
	this.stickerPropertiesDialog = new StickerPropertiesDialogViewModel(
		{
			isNewSticker : true
		});

	// helper function
	this.deleteRedundantBoardData = function(boardModel)
	{
		delete boardModel.stickerPropertiesDialog;
		_.each(boardModel.stickers, function(item)
		{
			delete item.isLinkVisible;
			item.top = item.styleBindings.top;
			item.left = item.styleBindings.left;
			item.zindex = item.styleBindings["z-index"];
			delete item.styleBindings;
		});
	}
};

BoardViewModel.prototype.createNewStickerClickHandler = function()
{
	this.trigger("onShowStickerPropertiesDialog");
	this.stickerPropertiesDialog.update(
		{
			isNewSticker : true
		});
};

BoardViewModel.prototype.updateBoard = function(boardData)
{
	_.each(
		boardData,
		function(sticker)
		{
			var stickerData = {};
			stickerData.id = sticker.RowKey;
			stickerData.additionalData = sticker.additionalData;
			stickerData.text = sticker.text;
			stickerData.url = sticker.url;
			stickerData.styleBindings = {};
			stickerData.styleBindings.top = sticker.top;
			stickerData.styleBindings.left = sticker.left;
			stickerData.styleBindings["z-index"] = sticker.zindex;

			window.boardViewModel.stickers.push(
				new StickerViewModel(stickerData));
		});
};

BoardViewModel.prototype.saveBoardClickHandler = function()
{
	var boardData = ko.toJS(this);
	this.deleteRedundantBoardData(boardData);

	this.storageClient.saveBoard(boardData);
};