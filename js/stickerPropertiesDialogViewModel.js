/*global browser:true, laxbreak:true, ko:false*/

var StickerPropertiesDialogViewModel = function(stickerModel)
{
	this.text = ko.observable();
	this.url = ko.observable();
	this.additionalData = ko.observable();
	this.isNewStickerMode = ko.observable();

	this.update(stickerModel);
};

StickerPropertiesDialogViewModel.prototype.update = function(dialogModel)
{
	this.text(dialogModel.text);
	this.url(dialogModel.url);
	this.additionalData(dialogModel.additionalData);
	this.isNewStickerMode(dialogModel.isNewSticker);
	this.stickerViewModel = dialogModel.stickerViewModel;
};

StickerPropertiesDialogViewModel.prototype.saveStickerButtonClickHandler = function()
{
	var stickerData = ko.toJS(this);

	if (this.isNewStickerMode())
	{
		stickerData.id = makeGuid();
		stickerData.styleBindings = {};
		stickerData.styleBindings.top = "30px";
		stickerData.styleBindings.left = "5px";
		stickerData.styleBindings["z-index"] = 10000;

		window.boardViewModel.stickers.push(
			new StickerViewModel(stickerData));
	}
	else
	{
		stickerData.id = stickerData.stickerViewModel.id;
		stickerData.styleBindings = stickerData.stickerViewModel.styleBindings;
		this.stickerViewModel.update(stickerData);
	}

	$("#stickerPropertiesDialog").dialog( "close" );
};
